Simple gruntfile that runs an external script in one task and uses files created by it in the second task.

To run, simply: 

`npm install`

`grunt`

In the end, you should have `assets/css/dummy.css` created by custom.js script which is run by the "One" task, as well as `swap/css/dummy.css` copied from the above by the "Two" task. 

To delete the two created directories:

`rm -rf assets swap`