'use strict';
var shell = require('shelljs');

module.exports = function(options) {
    
    console.log('Custom script is checking options...', options);

    options = options || {
        repo: options.repo
    };

    if ( !options ) { throw new Error('options is undefined'); }
    if ( !options.repo ) { throw new Error('options.find is undefined'); }
    if ( !options.path ) { throw new Error('options.path is undefined'); }

    console.log('Custom script generating CSS for repo: ', options.repo);

    // TODO
    
    shell.mkdir('-p', options.path);
    shell.touch([options.path, 'dummy.css'].join('/'));

    console.log('Custom script is done.');

};